<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sign in</title>
</head>
<body onload='document.login_form.login.focus();'>
<h3>Sign in</h3>
<form name="login_form" action="/login" method="post">
    <p>
        <label for="login">Login</label>
        <input id="login" name="login" type="text"></input>
    </p>
    <p>
        <label for="password">Password</label>
        <input id="password" name="password" type="password"></input>
    </p>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"></input>
    <button type="submit">Sign in</button>
</form>
</body>
</html>
