<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
>
<head>
    <title>Home</title>
</head>
<body>
<div th:if="${username}"> <!-- For example, ignore the error -->
    <p>${username}</p>
</div>
<form action="/login?logout" method="post"> <!-- Make /login?logout global constant -->
    <!-- CSRF authentication tokens -->
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
    <button type="submit">Logout</button>
</form>
</body>
</html>
