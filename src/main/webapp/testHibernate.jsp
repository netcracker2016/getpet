<%-- 
    Document   : testHibernate
    Created on : 09.03.2017, 12:04:57
    Author     : olegs
--%>

<%@page contentType="text/html" pageEncoding="windows-1251"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Date"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="com.netcracker.msk.model.hibernate.entities.User"%>
<%@page import="com.netcracker.msk.model.hibernate.dao.UserDAO"%>
<%@page import="com.netcracker.msk.model.hibernate.entities.Advert"%>
<%@page import="com.netcracker.msk.model.hibernate.dao.AdvertDAO"%>
<%@page import="com.netcracker.msk.model.hibernate.entities.Pet"%>
<%@page import="com.netcracker.msk.model.hibernate.dao.PetDAO"%>
<%@page import="com.netcracker.msk.model.hibernate.entities.Kind"%>
<%@page import="com.netcracker.msk.model.hibernate.dao.KindDAO"%>
<%@page import="com.netcracker.msk.model.hibernate.entities.Transaction"%>
<%@page import="com.netcracker.msk.model.hibernate.dao.TransactionDAO"%>
<%@page import="com.netcracker.msk.model.hibernate.HibernateUtil"%>
<%@page import="com.netcracker.msk.service.AdvertService"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Test hibernate</h1>
        <%
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getByUsername("user");
            out.println(user.getId());
        %>

    </body>
</html>
