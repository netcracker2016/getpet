package com.netcracker.msk.model.exceptions;

/**
 * Created by Dmitry Mihaylov on 5/17/17
 */

public class StorageFileNotFoundException extends StorageException {

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}