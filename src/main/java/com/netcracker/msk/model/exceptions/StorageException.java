package com.netcracker.msk.model.exceptions;

/**
 * Created by Dmitry Mihaylov on 5/17/17
 */
public class StorageException extends RuntimeException {

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
