package com.netcracker.msk.model.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;

/**
 * Created by Dmitry Mihaylov on 4/11/17
 */
public class RegistrationDTO {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

    private String email;

    private String phone;

    @DateTimeFormat(pattern="dd-MM-yyyy")
    private Date birthday;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
