/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.interfaces;


/**
 *
 * @author olegs
 * @param <E> тип сущности
 */
public interface DAOInterface<E> {

    E getById(int id);
    int create(E entity);
    void update (E entity);
    void delete (int id);
}
