/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.dao;

import com.netcracker.msk.model.hibernate.HibernateUtil;
import com.netcracker.msk.model.hibernate.entities.Card;
import com.netcracker.msk.model.hibernate.entities.Pet;
import com.netcracker.msk.model.interfaces.DAOInterface;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author olegs
 */
public class PetDAO extends AbstractDAO<Pet> {

    public PetDAO() {
    }

    @Override
    public void delete(int id) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from Pet where id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Pet getById(int id) {
        Pet pet = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from Pet where id = :id");
            query.setParameter("id", id);

            pet = ((List<Pet>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return pet;
    }

    public synchronized List<Pet> search(int page, int size, String kind, String breed, int minimalAge, int maximalAge) {
        List<Pet> pets;
        pets = new ArrayList<>();
        String sql;
        
        if (kind.isEmpty()) {
            if (breed.isEmpty()) {
                sql = "from Pet where age between " + minimalAge + " and " + maximalAge + "";
            } else {
                sql = "from Pet where breed = '" + breed + 
                    "' and age between " + minimalAge + " and " + maximalAge + "";
            }
        } else {
            if (breed.isEmpty()) {
                sql = "from Pet where kind = '" + kind + "'"
                        + " and age between " + minimalAge + " and " + maximalAge + "";
            } else {
                sql = "from Pet where kind = '" + kind + "' and breed = '" + breed + 
                    "' and age between " + minimalAge + " and " + maximalAge + "";
            }
        }

        sql = sql + "and id in (select petId from Advert where status = 'open') order by id";

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery(sql);
            query.setFirstResult((page - 1) * size);
            query.setMaxResults(size);

            pets = (List<Pet>) query.list();

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pets;
    }

    public synchronized List<Pet> getAllByOwnerId(int ownerId) {
        List<Pet> pets = new ArrayList<>();
        String sql = "from Pet where ownerId = " + ownerId;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery(sql);
            pets = (List<Pet>) query.list();

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pets;
    }

    public List<Pet> getLatest(int page, int size) {
        List<Pet> pets;
        pets = new ArrayList<>();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from Pet where id in (select petId from Advert where status = 'open') order by id");
            query.setFirstResult((page - 1) * size);
            query.setMaxResults(size);

            pets = (List<Pet>) query.list();

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pets;
    }
}
