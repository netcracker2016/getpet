package com.netcracker.msk.model.hibernate.entities;

/**
 * Created by saveta on 09.05.17.
 */

public class ClientAdvert {
    private ClientPet pet;
    private String text;
    private String date;

    public ClientAdvert(ClientPet pet, String text, String date) {
        this.pet = pet;
        this.text = text;
        this.date = date;
    }

    public ClientAdvert() {

    }

    public ClientPet getPet() {
        return pet;
    }

    public void setPet(ClientPet pet) {
        this.pet = pet;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
