package com.netcracker.msk.model.hibernate.dao;

import com.netcracker.msk.model.hibernate.HibernateUtil;
import com.netcracker.msk.model.interfaces.DAOInterface;
import org.hibernate.Session;

/**
 * Created by Dmitry Mihaylov on 4/30/17
 */
public abstract class AbstractDAO<E> implements DAOInterface<E> {

    @Override
    public int create(E entity) {
        int id = 0;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            java.io.Serializable assignedId;

            session.beginTransaction();
            assignedId = session.save(entity);
            session.getTransaction().commit();

            id = new Integer(assignedId.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }

    @Override
    public void update(E entity) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

}
