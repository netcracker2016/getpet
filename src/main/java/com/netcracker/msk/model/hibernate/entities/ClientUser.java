package com.netcracker.msk.model.hibernate.entities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ClientUser {
    private String login;
    private String email;
    private String phone;
    private String birthday;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "ClientUser{" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", birthday='" + birthday + '\'' +
                '}';
    }

    public ClientUser(User user) {
        this.login = user.getLogin();
        this.email = user.getEmail();
        this.phone = user.getPhone();

        if (user.getBirthday() != null) {
            LocalDate date = user.getBirthday().toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            this.birthday = date.format(formatter);
        }
    }

    public ClientUser() { }
}
