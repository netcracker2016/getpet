/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.dao;

import com.netcracker.msk.model.hibernate.HibernateUtil;
import com.netcracker.msk.model.hibernate.entities.Kind;
import com.netcracker.msk.model.interfaces.DAOInterface;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author olegs
 */
public class KindDAO extends AbstractDAO<Kind> {

    public KindDAO() {
    }

    @Override
    public void delete(int id) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from Kind where id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public Kind getById(int id) {
        Kind kind = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from Kind where id = :id");
            query.setParameter("id", id);

            kind = ((List<Kind>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return kind;
    }
    
    public List getKindsList() {
        List kinds = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("select kind from Kind");

            kinds = ((List<Kind>) query.list());

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return kinds;
    }
}
