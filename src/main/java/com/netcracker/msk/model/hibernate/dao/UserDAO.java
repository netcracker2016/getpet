/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.dao;

import com.netcracker.msk.model.hibernate.HibernateUtil;
import com.netcracker.msk.model.hibernate.entities.User;
import com.netcracker.msk.model.interfaces.DAOInterface;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author olegs
 */
public class UserDAO extends AbstractDAO<User> {

    public UserDAO() {
    }

    public UserDAO(SessionFactory sessionFactory) {
    }

    @Override
    public void delete(int id) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from User where id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getById(int id) {
        User user = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from User where id = :id");
            query.setParameter("id", id);

            if (query.list().isEmpty()) {
                return null;
            }

            user = ((List<User>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }

    public User verificationUser(String login, String password) {
        User user = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from User where login = :login and password = :password");
            query.setParameter("login", login);
            query.setParameter("password", password);

            user = ((List<User>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }

    public User getByUsername(String login) {
        User user = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from User where login = :login");
            query.setParameter("login", login);

            if (query.list().size() > 0) {
                user = ((List<User>) query.list()).get(0);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }
}
