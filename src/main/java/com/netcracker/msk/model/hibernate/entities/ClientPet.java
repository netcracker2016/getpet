package com.netcracker.msk.model.hibernate.entities;

/**
 * Created by saveta on 09.05.17.
 */

public class ClientPet {
    private String name;
    private String kind;
    private String breed;
    private Integer age;
    private String[] images;

    public ClientPet(String name, String kind, String breed, Integer age, String [] images) {
        this.name = name;
        this.kind = kind;
        this.breed = breed;
        this.age = age;
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public ClientPet() { }
}

