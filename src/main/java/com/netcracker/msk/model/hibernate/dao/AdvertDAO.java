/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.dao;

import com.netcracker.msk.model.hibernate.HibernateUtil;
import com.netcracker.msk.model.hibernate.entities.Advert;
import com.netcracker.msk.model.interfaces.DAOInterface;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author olegs
 */
public class AdvertDAO extends AbstractDAO<Advert> {

    public AdvertDAO() {
    }

    @Override
    public void delete(int id) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from Advert where id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Advert getById(int id) {
        Advert advert = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from Advert where id = :id");
            query.setParameter("id", id);

            advert = ((List<Advert>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return advert;
    }

    public Integer getIdByPetId(int petId) {
        Advert advert = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from Advert where petId = :petId");
            query.setParameter("petId", petId);

            advert = ((List<Advert>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (advert != null) {
            return advert.getId();
        } else {
            return null;
        }
    }

}
