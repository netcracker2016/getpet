/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.entities;

/**
 *
 * @author olegs
 */
public class Card {
    private String name;
    private String kind;
    private String breed;
    private int age;
    private String image;
    private int petId;
    private int advertId;

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of petId
     *
     * @return the value of petId
     */
    public int getPetId() {
        return petId;
    }

    /**
     * Set the value of petId
     *
     * @param petId new value of petId
     */
    public void setPetId(int petId) {
        this.petId = petId;
    }


    public Card() {
    }

    /**
     * Get the value of image
     *
     * @return the value of image
     */
    public String getImage() {
        return image;
    }

    /**
     * Set the value of image
     *
     * @param image new value of image
     */
    public void setImage(String image) {
        this.image = image;
    }


    /**
     * Get the value of age
     *
     * @return the value of age
     */
    public int getAge() {
        return age;
    }

    /**
     * Set the value of age
     *
     * @param age new value of age
     */
    public void setAge(int age) {
        this.age = age;
    }


    /**
     * Get the value of breed
     *
     * @return the value of breed
     */
    public String getBreed() {
        return breed;
    }

    /**
     * Set the value of breed
     *
     * @param breed new value of breed
     */
    public void setBreed(String breed) {
        this.breed = breed;
    }


    /**
     * Get the value of kind
     *
     * @return the value of kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * Set the value of kind
     *
     * @param kind new value of kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }
}
