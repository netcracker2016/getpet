/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.entities;

import java.sql.Date;

/**
 *
 * @author olegs
 */
public class Transaction implements java.io.Serializable {
    
    private int id;
    private int newOwnerId;
    private int exOwnerId;
    private int petId;
    private Date date;

    public Transaction() {
    }
    
    /**
     * Get the value of date
     *
     * @return the value of date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the value of date
     *
     * @param date new value of date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the value of petId
     *
     * @return the value of petId
     */
    public int getPetId() {
        return petId;
    }

    /**
     * Set the value of petId
     *
     * @param petId new value of petId
     */
    public void setPetId(int petId) {
        this.petId = petId;
    }

    /**
     * Get the value of exOwnerId
     *
     * @return the value of exOwnerId
     */
    public int getExOwnerId() {
        return exOwnerId;
    }

    /**
     * Set the value of exOwnerId
     *
     * @param exOwnerId new value of exOwnerId
     */
    public void setExOwnerId(int exOwnerId) {
        this.exOwnerId = exOwnerId;
    }

    /**
     * Get the value of newOwnerId
     *
     * @return the value of newOwnerId
     */
    public int getNewOwnerId() {
        return newOwnerId;
    }

    /**
     * Set the value of newOwnerId
     *
     * @param newOwnerId new value of newOwnerId
     */
    public void setNewOwnerId(int newOwnerId) {
        this.newOwnerId = newOwnerId;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }
}
