/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.entities;

/**
 *
 * @author olegs
 */
public class Kind implements java.io.Serializable {
    
    private int id;
    private String kind;

    public Kind() {
    }

    /**
     * Get the value of kind
     *
     * @return the value of kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * Set the value of kind
     *
     * @param kind new value of kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }
}
