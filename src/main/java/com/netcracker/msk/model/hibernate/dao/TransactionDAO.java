/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.dao;

import com.netcracker.msk.model.hibernate.HibernateUtil;
import com.netcracker.msk.model.hibernate.entities.Transaction;
import com.netcracker.msk.model.interfaces.DAOInterface;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author olegs
 */
public class TransactionDAO extends AbstractDAO<Transaction> {

    public TransactionDAO() {
    }

    @Override
    public void delete(int id) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from Transaction where id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public Transaction getById(int id) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("from Transaction where id = :id");
            query.setParameter("id", id);

            transaction = ((List<Transaction>) query.list()).get(0);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return transaction;
    }
}
