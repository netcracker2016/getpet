/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.model.hibernate.entities;

/**
 * @author olegs
 */
public class Pet implements java.io.Serializable {

    private int id;
    private String name;
    private String kind;
    private String breed;
    private int age;
    private int ownerId;
    private int given;
    private String images;

    public Pet() {
    }

    /**
     * Get the value of images
     *
     * @return the value of images
     */
    public String getImages() {
        return images;
    }

    /**
     * Set the value of images
     *
     * @param images new value of images
     */
    public void setImages(String images) {
        this.images = images;
    }

    /**
     * Get the value of given
     *
     * @return the value of given
     */
    public int getGiven() {
        return given;
    }

    /**
     * Set the value of given
     *
     * @param given new value of given
     */
    public void setGiven(int given) {
        this.given = given;
    }

    /**
     * Get the value of ownerId
     *
     * @return the value of ownerId
     */
    public int getOwnerId() {
        return ownerId;
    }

    /**
     * Set the value of ownerId
     *
     * @param ownerId new value of ownerId
     */
    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Get the value of age
     *
     * @return the value of age
     */
    public int getAge() {
        return age;
    }

    /**
     * Set the value of age
     *
     * @param age new value of age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Get the value of breed
     *
     * @return the value of breed
     */
    public String getBreed() {
        return breed;
    }

    /**
     * Set the value of breed
     *
     * @param breed new value of breed
     */
    public void setBreed(String breed) {
        this.breed = breed;
    }

    /**
     * Get the value of kind
     *
     * @return the value of kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * Set the value of kind
     *
     * @param kind new value of kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }

//    public boolean isGiven() {
//        if (this.given == 0) {
//            return false;
//        } else {
//            return true;
//        }
//    }

    public String[] getImagesList() {
        if (images != null) {
            return this.images.split("/");
        } else {
            return null;
        }
    }
    /*
    *
    * private int id;
    private String name;
    private String kind;
    private String breed;
    private int age;
    private int ownerId;
    private int given;
    private String images;*/

    public Pet(ClientPet clientPet, int ownerId) {
        this.name = clientPet.getName();
        this.kind = clientPet.getKind();
        this.breed = clientPet.getBreed();
        this.age = clientPet.getAge();
        this.given = 0;
        this.ownerId = ownerId;
        this.images = "";
        for (String filename: clientPet.getImages()) {
            if (!this.images.isEmpty())
                this.images += "/";
            this.images += filename;
        }
    }
}
