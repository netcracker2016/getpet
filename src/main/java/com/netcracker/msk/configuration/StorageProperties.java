package com.netcracker.msk.configuration;

import org.springframework.context.annotation.Configuration;

/**
 * Created by Dmitry Mihaylov on 5/17/17
 */

@Configuration
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location = "upload-dir";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}