package com.netcracker.msk.service;

import com.netcracker.msk.model.dto.RegistrationDTO;
import com.netcracker.msk.model.dto.UserInfoDTO;
import com.netcracker.msk.model.hibernate.dao.UserDAO;
import com.netcracker.msk.model.hibernate.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dmitry Mihaylov on 4/11/17
 */

@Service
public class UserService {

    private final UserFactory userFactory;
    private final UserDAO userDAO;

    @Autowired
    public UserService(UserFactory userFactory, UserDAO userDAO) {
        this.userFactory = userFactory;
        this.userDAO = userDAO;
    }

    public Integer createUser(RegistrationDTO registrationDTO) throws IllegalArgumentException, InternalError {
        User user = userFactory.createUser(registrationDTO);
        if (user != null) {
            User existingUser = getByUsername(user.getLogin());
            if (existingUser != null) {
                return null;
            }
            Integer id = userDAO.create(user);
            if (id != 0) {
                return id;
            } else {
                throw new InternalError("UserDao can't create new user. " +
                        "login: " + registrationDTO.getLogin() + ", " +
                        "password: " + registrationDTO.getPassword());
            }
        } else {
            throw new IllegalArgumentException("UserFactory can't create user from current registrationDTO." +
                    "login: " + registrationDTO.getLogin() + ", " +
                    "password: " + registrationDTO.getPassword());
        }
    }

    public String getUserInfo(String login) {
        System.out.println(login);
        User user = this.userDAO.getByUsername(login);
        return JsonService.createJson(user);
    }

    public UserInfoDTO getUserInfoById(int id) {
        User user = userDAO.getById(id);
        if (user == null) {
            return null;
        } else {
            return userFactory.extractInfo(user);
        }
    }

    public void update(String login) {
        User user = userDAO.getByUsername(login);
        if (user != null) {
            userDAO.update(user);
        }
    }

    public User getById(int id) {
        return userDAO.getById(id);
    }

    public User getByUsername(String username) {
        return userDAO.getByUsername(username);
    }

    public Integer getIdByUsername(String login) {
        User user = userDAO.getByUsername(login);
        if (user != null) return user.getId();
        return null;
    }
}
