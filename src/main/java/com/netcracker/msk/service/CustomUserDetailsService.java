package com.netcracker.msk.service;

import com.netcracker.msk.model.hibernate.dao.UserDAO;
import com.netcracker.msk.model.hibernate.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author alois9866
 */

@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {

    private UserDAO userDAO;

    @Autowired
    public CustomUserDetailsService(UserDAO userDAO) {
        super();
        this.userDAO = userDAO;
    }

    public CustomUserDetailsService() {
    }

    public UserDetails loadUserByUsername(String login)
            throws UsernameNotFoundException {

        User domainUser = userDAO.getByUsername(login);
        if (domainUser == null) {
            throw new UsernameNotFoundException(login + " not found");
        }

        return new org.springframework.security.core.userdetails.User(
                domainUser.getLogin(),
                domainUser.getPassword(),
                true,
                true,
                true,
                true,
                getAuthorities(domainUser.getRole())
        );
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    private Collection<GrantedAuthority> getAuthorities(String role) {
        List<String> roles = new ArrayList<>();
        roles.add(role);
        return getGrantedAuthorities(roles);
    }
}
