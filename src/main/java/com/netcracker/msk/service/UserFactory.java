package com.netcracker.msk.service;

import com.netcracker.msk.model.dto.RegistrationDTO;
import com.netcracker.msk.model.dto.UserInfoDTO;
import com.netcracker.msk.model.hibernate.entities.User;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;

/**
 * Created by Dmitry Mihaylov on 4/11/17
 */

@Service
public class UserFactory {

    public User createUser(RegistrationDTO registrationDTO) {
        User user;
        if (registrationDTO.getLogin() != null && registrationDTO.getPassword() != null){
            user = new User(registrationDTO.getLogin(), registrationDTO.getPassword());
        } else {
            return null;
        }

        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
        user.setRegistrationDate(currentDate);

        user.setRole("ROLE_USER");

        if (registrationDTO.getBirthday() != null) {
            user.setBirthday(registrationDTO.getBirthday());
        }
        if (registrationDTO.getEmail() != null) {
            user.setEmail(registrationDTO.getEmail());
        }
        if (registrationDTO.getPhone() != null) {
            user.setPhone(registrationDTO.getPhone());
        }

        return user;
    }

    public UserInfoDTO extractInfo(User user) {
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        Date birthday = user.getBirthday();
        if (birthday == null) {
            userInfoDTO.setBirthday("");
        } else {
            userInfoDTO.setBirthday(user.getBirthday().toString());
        }
        userInfoDTO.setLogin(user.getLogin());
        userInfoDTO.setEmail(user.getEmail());
        userInfoDTO.setId(user.getId());
        userInfoDTO.setPhone(user.getPhone());
        return userInfoDTO;
    }

}
