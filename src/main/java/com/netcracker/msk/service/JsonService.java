/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.service;

import com.google.gson.Gson;
import com.netcracker.msk.model.hibernate.entities.Advert;
import com.netcracker.msk.model.hibernate.entities.Card;
import com.netcracker.msk.model.hibernate.entities.ClientUser;
import com.netcracker.msk.model.hibernate.entities.User;

import java.util.List;

/**
 *
 * @author olegs
 */
public class JsonService {
    private static final Gson gson = new Gson();
    
    /**
     * Создание json и приведение его к православному виду  
     * @param cards
     * @return 
     */
    static public String createJson(List<Card> cards) {
//        String newJson;
//        newJson = "{\"data\":" + gson.toJson(cards) + "}";
        return "{\"data\":" + gson.toJson(cards) + "}";
    }

    static public String createJson(ClientUser user) {
        return "{\"data\":" + gson.toJson(user) + "}";
    }

    static public String createJson(User user) {
        return "{\"data\":" + gson.toJson(user) + "}";
    }

    static public String createJson(Advert advert) { return "{\"data\":" + gson.toJson(advert) + "}";  }

}
