/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.service;

import com.netcracker.msk.model.hibernate.dao.AdvertDAO;
import com.netcracker.msk.model.hibernate.dao.PetDAO;
import com.netcracker.msk.model.hibernate.entities.Advert;
import com.netcracker.msk.model.hibernate.entities.Pet;
import org.springframework.stereotype.Service;

/**
 *
 * @author olegs
 */

@Service
public class AdvertService {
    private final AdvertDAO advertDAO;
    private final PetDAO petDAO;

    public AdvertService(AdvertDAO advertDAO, PetDAO petDAO) {
        this.advertDAO = advertDAO;
        this.petDAO = petDAO;
    }
    
    public void create(Advert advert, Pet pet) {
        int petId;
        
        petId = petDAO.create(pet);
        advert.setPetId(petId);
        advertDAO.create(advert);
    }

    public void close(Advert advert) {
        if (advert != null) {
            advert.setStatus("close");
            advertDAO.update(advert);
        }
    }

    public void open(Advert advert) {
        if (advert != null) {
            advert.setStatus("open");
            advertDAO.update(advert);
        }
    }

    public Advert getById(int id) {
        return advertDAO.getById(id);
    }
    
}
