package com.netcracker.msk.service;

import com.netcracker.msk.model.dto.PetDTO;
import com.netcracker.msk.model.hibernate.dao.PetDAO;
import com.netcracker.msk.model.hibernate.entities.Pet;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dmitry Mihaylov on 4/30/17
 */

@Service
public class PetService {
    private final PetDAO petDAO;

    PetService(PetDAO petDAO) {
        this.petDAO = petDAO;
    }

    public Pet getById(int id) {
        return petDAO.getById(id);
    }

    public PetDTO getDTOById(int id) {
        Pet pet = getById(id);
        if (pet != null) {
            return petToPetDTO(pet);
        } else {
            return null;
        }
    }

    private PetDTO petToPetDTO(Pet pet) {
        PetDTO petDTO = new PetDTO();
        petDTO.setAge(pet.getAge());
        petDTO.setBreed(pet.getBreed());
        petDTO.setGiven(pet.getGiven());
        petDTO.setId(pet.getId());
        petDTO.setKind(pet.getKind());
        petDTO.setName(pet.getName());
        petDTO.setOwnerId(pet.getOwnerId());
        petDTO.setImages(pet.getImagesList());
        return petDTO;
    }

    public List<Pet> getAllByOwnerId(int ownerId) {
        return petDAO.getAllByOwnerId(ownerId);
    }
}
