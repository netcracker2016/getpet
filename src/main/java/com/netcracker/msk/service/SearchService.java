/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.service;

import com.netcracker.msk.model.hibernate.dao.AdvertDAO;
import com.netcracker.msk.model.hibernate.dao.KindDAO;
import com.netcracker.msk.model.hibernate.dao.PetDAO;
import com.netcracker.msk.model.hibernate.entities.Card;
import com.netcracker.msk.model.hibernate.entities.Pet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author olegs
 */

@Service
public class SearchService {
    private final PetDAO petDAO;
    private final KindDAO kindDAO;
    private final AdvertDAO advertDAO;

    public SearchService(PetDAO petDAO, KindDAO kindDAO, AdvertDAO advertDAO) {
        this.petDAO = petDAO;
        this.kindDAO = kindDAO;
        this.advertDAO = advertDAO;
    }
    
    /**
     * Receiving search results (in cards)
     * @param kind
     * @param breed
     * @param minimalAge
     * @param maximalAge
     * @param page
     * @param size
     * @return 
     */
    public String getSearchResults(String kind, String breed, int minimalAge, int maximalAge, int page, int size) {
        String output;
        List<Card> cards;
        List<Pet> pets;
        
        pets = petDAO.search(page, size, kind, breed, minimalAge, maximalAge);
        cards = this.convertPetsToCards(pets);
        Collections.reverse(cards);
        output = JsonService.createJson(cards);
        
        return output;
    }
    
    /** Receiving recently added pets (in cards) and converting it into JSON
     * Obtaining last adverts
     * @param page
     * @param size
     * @return 
     */
    public String getLatestPets(int page, int size) {
        String output;
        List<Card> cards;
        List<Pet> pets;
        
        pets = petDAO.getLatest(page, size);
        cards = this.convertPetsToCards(pets);
        Collections.reverse(cards);
        output = JsonService.createJson(cards);
        
        return output;
    }
    
    /**
     * This method create cards from pets
     * @param pets
     * @return 
     */
    public List<Card> convertPetsToCards(List<Pet> pets) {
        List<Card> cards;
        cards = new ArrayList<>();

        pets.forEach((Pet pet) -> {
            Card card = new Card();
            card.setKind(pet.getKind());
            card.setBreed(pet.getBreed());
            card.setAge(pet.getAge());
            String[] images = pet.getImagesList();
            if (images != null) {
                card.setImage((pet.getImagesList())[0]);
            } else {
                card.setImage(null);
            }
            card.setAdvertId(getAdvertId(pet));
            card.setPetId(pet.getId());
            card.setName(pet.getName());
            
            cards.add(card);
        });
        
        return cards;
    }
    
    /**
     * Recieving list of kinds
     * @return 
     */
    public String getKinds() {
        String kinds = "";
        List<String> kindsList;
        String separator = "/";
        
        kindsList = kindDAO.getKindsList(); 
        for (String kind : kindsList) {
            if (kinds.length() == 0) {
                kinds += kind;
            } else {
                kinds = kinds + separator + kind;
            }
        }
        
        return kinds;
    }

    private Integer getAdvertId(Pet pet) {
        return advertDAO.getIdByPetId(pet.getId());
    }
}
