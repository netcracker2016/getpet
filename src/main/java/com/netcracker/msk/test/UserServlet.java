package com.netcracker.msk.test;

import com.netcracker.msk.model.hibernate.dao.UserDAO;
import com.netcracker.msk.service.UserFactory;
import com.netcracker.msk.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UserServlet", urlPatterns = {"/user_servlet/*"})
public class UserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        response.addHeader("Access-Control-Allow-Origin", "*");
//        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
//        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");

        String login = request.getPathInfo().substring(1);

        UserService userService = new UserService(new UserFactory(), new UserDAO());
        String user = userService.getUserInfo(login);

        try (PrintWriter out = response.getWriter()) {
            out.println(user);
        } catch (Exception e) {
            System.out.println("Exception!");
        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //super.doPut(request, response);

        System.out.println("PUT REQUEST!");
        String pathInfo = request.getPathInfo().substring(1);
        String parts[] = pathInfo.split("/");
        System.out.println("parts[0]: " + parts[0] + "parts[1]:" + parts[1]);

        String login = parts[0];

        // FIXME: make userService common (as a member of the class)
//        UserService userService = new UserService(new UserFactory(), new UserDAO());
//        userService.update(login);

    }
}
