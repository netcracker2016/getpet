package com.netcracker.msk.controller;

import com.netcracker.msk.model.hibernate.dao.UserDAO;
import com.netcracker.msk.model.hibernate.entities.ClientUser;
import com.netcracker.msk.model.hibernate.entities.User;
import com.netcracker.msk.service.JsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value="/user")
public class UserController {

    private final UserDAO userDAO;

    @Autowired
    public UserController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @RequestMapping(value = "/{login}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String userInfo(@PathVariable String login) {
        User user = userDAO.getByUsername(login);
        ClientUser clientUser = new ClientUser(user);
        System.out.println(clientUser);
        return JsonService.createJson(clientUser);
    }

    @RequestMapping(value = "/{login}/edit", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> updateUser(@PathVariable String login, @RequestBody ClientUser user) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal.getClass() == org.springframework.security.core.userdetails.User.class) {
            org.springframework.security.core.userdetails.User springUser = (org.springframework.security.core.userdetails.User)
                    principal;
            if (springUser.getUsername().equals(user.getLogin())) {

                User dbUser = userDAO.getByUsername(login);
                dbUser.setLogin(user.getLogin());
                dbUser.setEmail(user.getEmail());

                if (user.getBirthday() != null) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                    LocalDate date = LocalDate.parse(user.getBirthday(), formatter);
                    dbUser.setBirthday(java.sql.Date.valueOf(date));
                }
                else dbUser.setBirthday(null);

                dbUser.setPhone(user.getPhone());
                userDAO.update(dbUser);
                return ResponseEntity.ok(JsonService.createJson(new ClientUser(userDAO.getByUsername(login))));
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);

    }

}
