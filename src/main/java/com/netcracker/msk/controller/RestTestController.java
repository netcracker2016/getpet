package com.netcracker.msk.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Dmitry Mihaylov on 4/6/17
 */
@RestController
@RequestMapping(value = "/api/test")
public class RestTestController {

    @RequestMapping(method = RequestMethod.POST)
    public String sendHello() {
        return "Hello post request!";
    }

}
