package com.netcracker.msk.controller;

import com.netcracker.msk.model.hibernate.dao.AdvertDAO;
import com.netcracker.msk.model.hibernate.dao.PetDAO;
import com.netcracker.msk.model.hibernate.dao.UserDAO;
import com.netcracker.msk.model.hibernate.entities.Advert;
import com.netcracker.msk.model.hibernate.entities.ClientAdvert;
import com.netcracker.msk.model.hibernate.entities.ClientPet;
import com.netcracker.msk.model.hibernate.entities.Pet;
import com.netcracker.msk.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class AddAdvertController {

    private final AdvertService advertService;
    private final UserService userService;

    @Autowired
    public AddAdvertController() {
        this.advertService = new AdvertService(new AdvertDAO(), new PetDAO());
        this.userService = new UserService(new UserFactory(), new UserDAO());
    }

    @RequestMapping(value = "/add_poster", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> addAdvert(@RequestBody ClientAdvert clientAdvert) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal.getClass() == User.class) {
            User springUser = (User) principal;
            String username = springUser.getUsername();
            Integer userId = userService.getIdByUsername(username);
            if (userId == null) return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);

            ClientPet clientPet = clientAdvert.getPet();
            Pet pet = new Pet(clientPet, userId);
            Advert advert = new Advert(clientAdvert, userId);

            advertService.create(advert, pet);
            return ResponseEntity.ok("Advert added");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

}
