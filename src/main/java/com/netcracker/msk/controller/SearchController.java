/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.msk.controller;

import com.netcracker.msk.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author olegs
 */
@CrossOrigin
@RestController
@RequestMapping(value="search")
public class SearchController {

    private  SearchService searchService;
    
    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }
    
    @RequestMapping(value = "latest", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String latest(@RequestParam("size") int size, @RequestParam("page") int page) {
        String latest;
        latest = this.searchService.getLatestPets(page, size);
        return latest;
    }
    
    @RequestMapping(value = "kinds", method = RequestMethod.GET)
    @ResponseBody
    public String kinds() {
        String kinds;
        kinds = this.searchService.getKinds();
        return kinds;
    }
    
    @RequestMapping(value = "search", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String search(
            @RequestParam("size") int size,
            @RequestParam("page") int page,
            @RequestParam("kind") String kind,
            @RequestParam("breed") String breed,
            @RequestParam("min_age") int minAge, 
            @RequestParam("max_age") int maxAge) {
        String results;
        
        if (breed == null) {
            breed = "";
        }
        
        if (kind == null) {
            breed = "";
        }
        
//        if (maxAge == 0) {
//            maxAge = 100500;
//        }

        results = this.searchService.getSearchResults(kind, breed, minAge, maxAge, page, size);
        return results;
    }
}
