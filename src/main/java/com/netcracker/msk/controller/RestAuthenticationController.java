package com.netcracker.msk.controller;

import com.netcracker.msk.model.dto.LoginDTO;
import com.netcracker.msk.model.dto.RegistrationDTO;
import com.netcracker.msk.service.CustomUserDetailsService;
import com.netcracker.msk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Dmitry Mihaylov
 */

@RestController
@RequestMapping(value = "/api")
public class RestAuthenticationController {

    private final CustomUserDetailsService customUserDetailsService;
    private final UserService userDAOService;

    @Autowired
    public RestAuthenticationController(CustomUserDetailsService customUserDetailsService,
                                        UserService userDAOService) {
        this.customUserDetailsService = customUserDetailsService;
        this.userDAOService = userDAOService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public UserDetails login(@RequestBody LoginDTO loginDTO, HttpServletResponse response) {

        if (loginDTO.getLogin() == null || loginDTO.getPassword() == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        UserDetails user;

        try {
            user = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        } catch (UsernameNotFoundException exception) {
            System.err.println(exception.getMessage());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

        if (user.getPassword().equals(loginDTO.getPassword())) {
            Authentication auth =
                    new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);
            response.setStatus(HttpServletResponse.SC_OK);
            return user;
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public void register(@RequestBody RegistrationDTO registrationDTO, HttpServletResponse response) {
        try {
            Integer id = userDAOService.createUser(registrationDTO);
            if (id == null) {
                response.setStatus(HttpServletResponse.SC_CONFLICT);
                return;
            }
        } catch (InternalError e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public void testIfAuthenticated(@RequestParam("login") String login,
                                    HttpServletResponse response) throws IOException {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal.getClass() == User.class) {
            User springUser = (User) principal;
            if (!springUser.getUsername().equals(login)) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            } else {
                response.setStatus(HttpServletResponse.SC_OK);
                return;
            }
        }
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

}
