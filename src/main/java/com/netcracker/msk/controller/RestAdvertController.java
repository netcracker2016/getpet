package com.netcracker.msk.controller;

import com.netcracker.msk.model.dto.PetDTO;
import com.netcracker.msk.model.dto.UserInfoDTO;
import com.netcracker.msk.model.hibernate.dao.AdvertDAO;
import com.netcracker.msk.model.hibernate.entities.Advert;
import com.netcracker.msk.model.hibernate.entities.Card;
import com.netcracker.msk.model.hibernate.entities.Pet;
import com.netcracker.msk.model.hibernate.entities.User;
import com.netcracker.msk.service.AdvertService;
import com.netcracker.msk.service.PetService;
import com.netcracker.msk.service.SearchService;
import com.netcracker.msk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry Mihaylov on 4/27/17
 */
@RestController
@RequestMapping(value = "/api")
public class RestAdvertController {
    private final AdvertService advertService;
    private final UserService userService;
    private final PetService petService;
    private final SearchService searchService;

    @Autowired
    public RestAdvertController(AdvertService advertService,
                                UserService userService,
                                PetService petService,
                                SearchService searchService) {
        this.advertService = advertService;
        this.userService = userService;
        this.petService = petService;
        this.searchService = searchService;
    }

    @RequestMapping(value = "/poster", method = RequestMethod.GET)
    public Advert getAdvert(@RequestParam("id") int id, HttpServletResponse response) {
        Advert advert = advertService.getById(id);
        if (advert == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } else {
            return advert;
        }

    }

    @RequestMapping(value = "/poster/status", method = RequestMethod.POST)
    public void setStatusAdvert(@RequestParam("id") int id, @RequestParam("status") String status,
                                HttpServletResponse response) {
        Advert advert = advertService.getById(id);
        if (advert == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        User user = userService.getById(advert.getAuthorId());
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal.getClass() == org.springframework.security.core.userdetails.User.class) {
            org.springframework.security.core.userdetails.User springUser = (org.springframework.security.core.userdetails.User)
                    principal;
            if (springUser.getUsername().equals(user.getLogin())) {
                switch (status) {
                    case "open":
                        advertService.open(advert);
                        break;
                    case "close":
                        advertService.close(advert);
                        break;
                    default:
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                }
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
    }


    @RequestMapping(value = "/userinfo", method = RequestMethod.GET)
    public UserInfoDTO getUserInfo(@RequestParam("id") int id, HttpServletResponse response) {
        UserInfoDTO userInfoDTO = userService.getUserInfoById(id);
        if (userInfoDTO == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } else {
            return userInfoDTO;
        }
    }

    @RequestMapping(value = "/pet", method = RequestMethod.GET)
    public PetDTO getPetById(@RequestParam("id") int id, HttpServletResponse response) {
        PetDTO petDTO = petService.getDTOById(id);
        if (petDTO == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } else {
            return petDTO;
        }
    }

    @RequestMapping(value = "/pet/list", method = RequestMethod.GET)
    public List<Card> getPetsByLogin(@RequestParam("login") String login, HttpServletResponse response) {
        User user = userService.getByUsername(login);
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        int id = user.getId();
        List<Pet> pets = petService.getAllByOwnerId(id);
        return searchService.convertPetsToCards(pets);
    }
}
