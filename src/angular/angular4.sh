#!/bin/bash
npm -S -E i @angular/{common,compiler,core,forms,http,platform-browser,platform-browser-dynamic,platform-server,router}@next typescript@latest
npm --D -E i @angular/compiler-cli@next
npm --save install @ng-bootstrap/ng-bootstrap
npm --save install ng2-file-uploader