import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {TestComponent} from "./components/test/test.component";
import {AuthGuard} from "./services/auth-guard.service";

import {InfoComponent} from "./components/info/info.component";
import { UserEditComponent } from "./components/edit/user-edit.component";
import { HomeComponent } from "./components/home/home.component";
import {PosterComponent} from "./components/poster/poster.component";
import {ContentComponent} from "./components/content/content.component";
import {AboutComponent} from "./components/about/about.component";
//import {AdvertComponent} from "./components/advert/advert.component";
import {LoginService} from "./services/login.service";
import {AddPosterComponent} from "./components/add_poster/add-poster.component";

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'test', component: TestComponent, canActivate: [AuthGuard]},
    //{path: 'info', redirectTo: `/info/${LoginService.getUserLogin()}`, pathMatch: 'full', canActivate: [AuthGuard] },
    {path: 'info/:login', component: InfoComponent, canActivate: [AuthGuard] },
    {path: 'info/:login/edit', component: UserEditComponent, canActivate: [AuthGuard] },
    {path: 'home', component: HomeComponent},
    {path: 'poster/:id', component: PosterComponent},
    {path: 'about', component: AboutComponent},
    {path: 'search', component: ContentComponent},
    {path: '', component: ContentComponent},
    {path: 'add_advert', component: AddPosterComponent, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
