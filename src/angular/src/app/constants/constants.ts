export class Constants {
    public static serverUrl: string = 'http://localhost:8080/';
    public static separator = '/';
    public static size = 8; // Number of items per page
    public static title = 'Get Pet';
}