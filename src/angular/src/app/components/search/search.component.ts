import {Component, EventEmitter, Output} from "@angular/core";

import {Card} from "../../entities/card";
import {SearchService} from "../../services/search.service";
import {SearchQuery} from "../../entities/SearchQuery";

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
})
export class SearchComponent {
    kinds: string[] = ["error"];
    kind: string;
    breed: string;
    minimalAge: number;
    maximalAge: number;

    error: boolean;

    cards: Card[] = [];
    @Output() onContentChanged = new EventEmitter<Card[]>();

    constructor(private contentService: SearchService) {

    }

    ngOnInit() {
        this.contentService.getKinds().then(data => {
            this.kinds = data;
        });
    }

    search(kind: string, breed: string, minimalAge: number, maximalAge: number) {
        SearchQuery.reset();
        this.error = false;
        this.cards = [];

        if (kind === null || kind === undefined) {
            kind = "";
        }

        if (breed === null || breed === undefined) {
            breed = "";
        }

        if (minimalAge === null || minimalAge === undefined) {
            minimalAge = 0;
        }

        if (maximalAge === null || maximalAge === undefined) {
            maximalAge = 100500;
        }

        if (maximalAge < 0 || minimalAge < 0 || minimalAge > maximalAge) {
            this.error = true;
            SearchQuery.error = true;
        } else {
            SearchQuery.kind = kind;
            SearchQuery.breed = breed;
            SearchQuery.minimalAge = minimalAge;
            SearchQuery.maximalAge = maximalAge;

            this.getResults();
        }
    }

    private getResults() {

        this.contentService.getSearchResults().then(data => {
            this.cards = data;
            this.onContentChanged.emit(this.cards);
        });
    }
}
