import {Component, Input} from "@angular/core";
import {Card} from "../../entities/card";
import {SearchService} from "../../services/search.service";
import {SearchQuery} from "../../entities/SearchQuery";
// declare var $:JQueryStatic;

@Component({
    selector: 'results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.css'],
})
export class ResultsComponent {
    @Input() cards: Card[];
    error: boolean = SearchQuery.error;

    constructor(private contentService: SearchService) {

    }

    ngAfterContentInit() {
        let _this = this;
        $(window).scroll(function()
        {
            if  ($(window).scrollTop() == $(document).height() - $(window).height())
            {
                _this.moreResults();
            }
        });
    }

    ngAfterContentChecked() {
        /**
         * Hide results if the input is incorrect
         * @type {boolean}
         */
        this.error = SearchQuery.error;
    }

    moreResults() {
        SearchQuery.increasePage();
        this.contentService.getSearchResults()
            .then(data => {this.cards = this.cards.concat(data)});
    }
}