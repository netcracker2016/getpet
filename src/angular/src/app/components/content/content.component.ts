import {Component} from "@angular/core";
import {Card} from "../../entities/card";
import {SearchService} from "../../services/search.service";
import {SearchQuery} from "../../entities/SearchQuery";

@Component({
    selector: 'content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.css'],
})
export class ContentComponent {
    cards: Card[] = [];

    constructor(private contentService: SearchService) {

    }

    ngOnInit() {
        SearchQuery.reset();
        this.contentService.getLatest().then(data => this.cards = data);
    }

    onContentChanged(cards: Card[]) {
        this.cards = cards;
    }
}