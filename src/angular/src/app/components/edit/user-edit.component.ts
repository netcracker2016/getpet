import {Component, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {User} from "../../entities/user";
import {UserService} from "../../services/user.service";
import {Router, ActivatedRoute} from "@angular/router";

declare var jQuery: any;

@Component({
    selector: 'user-edit',
    templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit, OnDestroy {

    user : User;
    private sub: any;
    private login: string;
    @ViewChild('input') input: ElementRef;

    constructor(private service: UserService, private router: Router, private activatedRoute: ActivatedRoute) { }

    ngAfterViewInit() {
        jQuery(this.input.nativeElement).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: new Date(1920, 1, 1),
            maxDate: '-1Y',
            onSelect: (value: string) => {
                this.user.birthday = value;
            }

        });
    }

    ngOnInit() {
        this.user = new User('', '', '', '');
        this.sub = this.activatedRoute.params.subscribe(params => {
            this.login = params['login'];
            this.service.getUserInfo(this.login)
              .then(user => this.user = user);
        });
    }

    save() {
        this.service.update(this.user)
          .then((result) => this.router.navigate(['/info', this.user.login]));
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}
