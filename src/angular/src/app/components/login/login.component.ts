import {Component} from "@angular/core";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {LoginService} from "../../services/login.service";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
})
export class LoginComponent {

    public login: string = "";
    public password: string = "";
    public message: string = "";

    constructor(private loginService: AuthenticationService, private router: Router) {
    }

    onSubmit(): void {
        this.loginService
            .login(this.login, this.password)
            .then(
                answer => {
                    switch (answer) {
                        case 200:
                            this.message = "Success";
                            LoginService.setUserLogin(this.login);
                            this.router.navigate(['']).then();
                            break;
                        case 401:
                            this.message = "Login or password is incorrect.";
                            break;
                        default:
                            this.message = "Something went wrong.";
                            break;
                    }
                }
            );
    }
}
