import {Component, DoCheck, OnInit} from "@angular/core";
import {Constants} from "../../constants/constants";
import {AuthenticationService} from "../../services/authentication.service";
import {LoginService} from "../../services/login.service";
import {Router} from "@angular/router";

@Component({
    selector: 'navigation',
    styleUrls: ['./navigation.component.css'],
    templateUrl: './navigation.component.html'
})
export class NavigationComponent implements DoCheck, OnInit {
    title = Constants.title;
    auth: boolean = false;
    login: string;

    constructor(private authenticationService: AuthenticationService,
                private router: Router,
                private loginService: LoginService) {
    }

    ngOnInit(): void {
        this.loginService.checkServerIfLogin();
    }

    logout(): void {
        this.authenticationService.logout().then(
            answer => {
                if (answer === 404) {
                    LoginService.logout();
                    this.router.navigate(['']).then();
                } else {
                    console.log("Logout error: something wrong.");
                }
            }
        )
    }

    ngDoCheck(): void {
        this.auth = LoginService.isAuthenticated();
        this.login = LoginService.getUserLogin();
    }
}