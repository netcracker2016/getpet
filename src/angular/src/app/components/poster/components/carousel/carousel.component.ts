/**
 * Created by Dmitry Mihaylov on 4/19/17.
 */

import {Component, Input} from "@angular/core";
@Component({
    selector: 'carousel',
    styleUrls: ['/carousel.component.css'],
    templateUrl: './carousel.component.html'

})
export class Carousel {
    @Input() public slides: string[];

}