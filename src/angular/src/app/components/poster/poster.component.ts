/**
 * Created by Dmitry Mihaylov on 4/19/17.
 */

import {Component, OnInit} from "@angular/core";
import {PosterService} from "../../services/poster.service";
import {Poster} from "../../entities/poster";
import {UserInfo} from "../../entities/user-info";
import {ActivatedRoute} from "@angular/router";
import {Response} from "@angular/http";
import {Pet} from "../../entities/pet";
import {LoginService} from "../../services/login.service";
import {Constants} from "../../constants/constants";

@Component({
    selector: 'poster',
    templateUrl: './poster.component.html',
    styleUrls: ["./poster.component.css"]
})
export class PosterComponent implements OnInit {

    private imageUrlPrefix = Constants.serverUrl + "/files/";

    private poster: Poster;
    private userInfo: UserInfo;
    private pet: Pet;

    public loginLocalStorage: string;

    public slides: string[] = [];
    public id: number;
    public login = "Loading...";
    public phone = "Loading...";
    public email = "Loading...";
    public info = "Loading...";
    public status = "open";

    constructor(private posterService: PosterService, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.loginLocalStorage = LoginService.getUserLogin();
        this.activatedRoute.params.subscribe(
            params => {
                let id = params['id'];
                if (id) {
                    this.posterService.getPoster(id).subscribe(
                        response => {
                            this.parsePoster(response, this)
                        },
                        this.catchError
                    )
                }
            }
        );
    }

    parsePoster(response: Response, posterComponent: PosterComponent): void {
        posterComponent.poster = <Poster> response.json();
        if (posterComponent.poster) {
            posterComponent.info = posterComponent.poster.text;
            posterComponent.status = posterComponent.poster.status;
            posterComponent.id = posterComponent.poster.id;
            posterComponent.posterService.getUserInfo("" + posterComponent.poster.authorId).subscribe(
                response => {
                    posterComponent.parseUserInfo(response, posterComponent)
                },
                this.catchError
            );
            posterComponent.posterService.getPet("" + posterComponent.poster.petId).subscribe(
                response => {
                    posterComponent.parsePet(response, posterComponent)
                },
                this.catchError
            )
        }
    }

    parseUserInfo(response: Response, posterComponent: PosterComponent) {
        posterComponent.userInfo = <UserInfo> response.json();
        if (posterComponent.userInfo) {
            posterComponent.phone = posterComponent.userInfo.phone;
            posterComponent.email = posterComponent.userInfo.email;
            posterComponent.login = posterComponent.userInfo.login;
        }
    }

    parsePet(response: Response, posterComponent: PosterComponent) {
        posterComponent.pet = <Pet> response.json();
        posterComponent.slides = [];
        if (posterComponent.pet) {
            for(let i in posterComponent.pet.images) {
                posterComponent.slides[i] = this.imageUrlPrefix + posterComponent.pet.images[i];
            }
        }
    }

    catchError(e: any): void {
        console.error(e);
    }

    changeStatus() {
        if (this.status === "open") {
            this.closePoster();
            return;
        } else {
            this.openPoster();
            return;
        }
    }

    closePoster() {
        this.posterService.updateStatusAdvert(this.id, "close").subscribe(
            () => {
                this.status = "close";
            },
            this.catchError
        )
    }

    openPoster() {
        this.posterService.updateStatusAdvert(this.id, "open").subscribe(
            () => {
                this.status = "open";
            },
            this.catchError
        )
    }

}
