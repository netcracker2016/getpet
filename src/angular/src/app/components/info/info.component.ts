import {Component, OnDestroy, OnInit} from "@angular/core";
import {User} from "../../entities/user";
import {UserService} from "../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LoginService} from "../../services/login.service";
import {Card} from "../../entities/card";
import {Constants} from "../../constants/constants";

@Component({
    selector: 'info',
    styleUrls: ['./info.component.css'],
    templateUrl: './info.component.html',
})
export class InfoComponent implements OnInit, OnDestroy {

    public imageUrlPrefix = Constants.serverUrl + "/files/";

    user: User;
    private sub: any;
    loginUrl: string;
    loginLocalStorage: string;
    public pets: Card[] = [];

    constructor(private service: UserService, private router: Router, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.loginLocalStorage = LoginService.getUserLogin();
        this.user = new User('', '', '', '');
        this.sub = this.activatedRoute.params.subscribe(params => {
            this.loginUrl = params['login'];
        });

        this.service
            .getUserInfo(this.loginUrl)
            .then(user => {
                this.user = user;
            })
            .catch(err => console.log(err.message || err));

        this.getPets();
    }

    edit(): void {
        this.router.navigate(['/info', this.user.login, 'edit']);
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    getPets(): void {
        this.service.getPets(this.loginUrl).subscribe(
            response => {
                this.pets = response.json() as Card[];
            },
            error => {
                console.error(error);
            }
        )
    }

}
