/**
 * Created by Dmitry Mihaylov on 4/9/17.
 */

import {Component} from "@angular/core";
@Component({
    selector: 'home',
    template: `
        <div>
            <h1> Home Page </h1>
        </div>
    `
})
export class HomeComponent {

}