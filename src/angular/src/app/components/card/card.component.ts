import { Component, Input, OnInit } from '@angular/core';

import { Card } from '../../entities/card';
import {Constants} from "../../constants/constants";

@Component({
    selector: 'card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
    public imageUrlPrefix = Constants.serverUrl + "/files/";
    
    @Input() card: Card;

    ngOnInit(): void {
        let image = $('.card-image');
        let width = image.width();
        image.css({
            'height': width + 'px'
        });
    }
}