import {AfterViewChecked, Component, ViewChild} from "@angular/core";
import {AuthenticationService} from "../../services/authentication.service";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'registration',
    styleUrls: ['./registration.component.css'],
    templateUrl: './registration.component.html',
})
export class RegistrationComponent implements AfterViewChecked {

    public message: string = "";

    constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    registrationDTO = {
        login: '',
        password: '',
        email: '',
        phone: '',
        birthday: null as NgbDateStruct
    };

    formErrors = {
        'login': '',
        'password': '',
        'confirmPassword': '',
        'email': '',
        'phone': '',
        'birthday': ''
    };

    validationMessages = {
        'login': {
            'required': 'Login is required.',
            'minlength': 'Login must be at least 4 characters long.',
            'maxlength': 'Login cannot be more than 24 characters long.',
        },
        'password': {
            'required': 'Password is required.',
            'minlength': 'Password must be at least 8 characters long.',
            'maxlength': 'Password cannot be more than 100 characters long.',
        },
        'confirmPassword': {
            'required': 'Confirm your password.',
        },
        'email': {
            'required': 'Email is required.',
        },
        '': {
            'required': 'Phone is required.',
        },
        'birthday': {
            'required': 'Birthday is required.',
        }
    };

    private registrationForm: NgForm;
    @ViewChild('registrationForm') currentForm: NgForm;

    ngAfterViewChecked() {
        this.formChanged()
    }

    formChanged() {
        if (this.registrationForm === this.currentForm) {
            return;
        }
        this.registrationForm = this.currentForm;
        if (this.registrationForm) {
            this.registrationForm.valueChanges.subscribe(
                data => {
                    this.onValueChanged(data)
                }
            )
        }
    }

    onValueChanged(data?: any) {
        if (!this.registrationForm) {
            return;
        }
        const form = this.registrationForm.form;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    onSubmit(): void {
        let date: string;
        if (this.registrationDTO.birthday != null) {
            date = this.registrationDTO.birthday.day + "-" +
                this.registrationDTO.birthday.month + "-" +
                this.registrationDTO.birthday.year;
        } else {
            date = null;
        }
        this.authenticationService.register(
            this.registrationDTO.login,
            this.registrationDTO.password,
            this.registrationDTO.email,
            this.registrationDTO.phone,
            date
        ).subscribe(
            response => {
                this.router.navigate(['']).then();
            },
            error => {
                switch (error.status) {
                    case 409:
                        this.message = "User with this username already exist.";
                        break;
                    default:
                        console.log(error);
                        break;
                }
            }
        )
    }
}
