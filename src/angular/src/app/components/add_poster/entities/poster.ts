/**
 * Created by saveta on 08.05.17.
 */

/*
* CREATE TABLE `pet` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(45) DEFAULT NULL,
 `kind` varchar(45) NOT NULL,
 `breed` varchar(45) DEFAULT NULL,
 `age` int(11) DEFAULT '-1',
 `given` tinyint(1) NOT NULL DEFAULT '0',
 `owner_id` int(11) NOT NULL,
 `images` varchar(45) DEFAULT NULL,
 PRIMARY KEY (`id`),
 * */

/*
* CREATE TABLE `advert` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `author_id` int(11) NOT NULL,
 `text` varchar(200) NOT NULL,
 `status` varchar(10) NOT NULL,
 `start_date` datetime NOT NULL,
 `pet_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 * */
class Pet {
  name: string;
  kind: string;
  breed: string;
  age: number;
  images: string[];

  constructor(name: string, kind: string, breed: string, age: number, images: string[]) {
    this.name = name;
    this.kind = kind;
    this.breed = breed;
    this.age = age;
    this.images = images;
  }
}

export class Advert {
  pet: Pet;
  text: string;
  date: string;

  constructor(name: string, kind: string, breed: string, age: number, text: string, date: string, images: string[]) {
    this.pet = new Pet(name, kind, breed, age, images);
    this.text = text;
    this.date = date;
  }

}