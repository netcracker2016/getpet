/**
 * Created by saveta on 08.05.17.
 */
import {Component, OnInit, ViewChild} from "@angular/core";
import {Advert} from "./entities/poster";
import {SearchService} from "../../services/search.service";
import {AddAdvertService} from "../../services/add-poster.service";
import {Router} from "@angular/router";
import {FileUploadComponent} from "./components/file-upload/file-upload.component";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'add-advert',
    templateUrl: './add-poster.component.html',
})
export class AddPosterComponent implements OnInit {

    kinds: string[] = ["error"];
    // FIXME: that's because of i can't upload to localhost.
    images: string[] = [];
    advert: Advert = new Advert('', '', '', 0, '', '', this.images);

    public startUpload: boolean = false;

    @ViewChild(FileUploadComponent)
    private fileUploadComponent: FileUploadComponent;

    constructor(private searchService: SearchService, private advertService: AddAdvertService, private router: Router) {
    }

    // FIXME: can i use it?
    ngOnInit(): void {
        this.searchService.getKinds().then(data => {
            this.kinds = data;
            this.advert.pet.kind = this.kinds[0];
        });
    }

    saveDateAndStartImageUpload() {

        // FIXME: change date format properly
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        let minutes = date.getMinutes();
        let hours = date.getHours();
        let seconds = date.getSeconds();
        this.advert.date = day + "-" + month + "-" + year + " " + hours + ":" + minutes + ":" + seconds;

        this.startUpload = true;
    }

    onUpload(results: Observable<string>): void {
        results.subscribe(
            (next) => {
                this.images.push(next);
            },
            (error) => {
                console.error(error);
            },
            () => {
                this.uploadPoster();
            }
        )
    }

    uploadPoster(): void {
        this.startUpload = false;
        console.log(this.images);
        console.log(this.advert);
        this.advert.pet.images = this.images;

        this.advertService.saveAdvert(this.advert)
            .then((result) => this.router.navigate(['']));
    }


}