/**
 * Created by saveta on 10.05.17.
 */
import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {FileItem, FileUploader} from "ng2-file-upload";
import {Constants} from "../../../../constants/constants";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

const URL = Constants.serverUrl + "upload";

//const headers: Headers = new Headers({'Content-Type': 'multipart/form-data'});

@Component({
    selector: 'file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['/.file-upload.component.css']
})
export class FileUploadComponent implements OnChanges {
    public uploader: FileUploader = new FileUploader({url: URL});
    public hasBaseDropZoneOver: boolean = false;

    private _results: Subject<string>;
    private results: Observable<string>;

    @Output() onUpload = new EventEmitter<Observable<string>>();
    @Input() start: boolean = false;

    ngOnChanges(changes: SimpleChanges): void {
        if (this.start) {
            this.upload();
        }
    }

    public fileOverBase(e: boolean): void {
        this.hasBaseDropZoneOver = e;
    }

    public upload(): void {
        this._results = new Subject();
        this.results = this._results.asObservable();

        this.uploader.onBeforeUploadItem = (item: FileItem) => {
            item.withCredentials = false;
        };

        this.uploader.onSuccessItem = (item: FileItem, response: string) => {
            this._results.next(response);
        };

        this.uploader.onCompleteAll = () => {
            this._results.complete();
        };

        this.uploader.uploadAll();

        this.onUpload.emit(this.results);
    }

}