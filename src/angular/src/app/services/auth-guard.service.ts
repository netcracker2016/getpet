import {Injectable} from '@angular/core';
import {LoginService} from "./login.service";

import { CanActivate, Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate() {
        if (LoginService.isAuthenticated())
            return true;
        this.router.navigate(['/login']);
        return false;
    }
}