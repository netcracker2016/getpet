
import { Injectable } from "@angular/core";
import { Http, Response, Headers } from '@angular/http';
import { Card } from "../entities/card";
import 'rxjs/add/operator/toPromise';
import { Constants } from "../constants/constants";
import { Page } from "../entities/page";

@Injectable()
export class ContentService {
    loading: boolean

    constructor(private http: Http) {

    }

    public getSearchResults(kind: string, breed: string, minimalAge: number, maximalAge: number): Promise<Card[]> {
        // const url: string = this.serverUrl + '&type=search&kind=' + kind + '&breed=' + breed + 
        //     '&minimal_age=' + minimalAge + '&maximal_age=' + maximalAge;
        // const url = Constants.serverUrl + 'TestServlet' + '?page=' + Page.get() + '&size=' + Constants.size + '&type=search&kind=' + kind + '&breed=' + breed + 
        //     '&minimal_age=' + minimalAge + '&maximal_age=' + maximalAge;
        const url = Constants.serverUrl + 'content/search' + '?page=' + Page.get() + '&size=' + Constants.size + '&kind=' + kind + '&breed=' + breed + 
            '&min_age=' + minimalAge + '&max_age=' + maximalAge;
        this.loading = true;

        console.log('URL >>> ' + url);
        return this.http.get(url)
                        .toPromise()
                        .then(response => {
                            this.loading = false;
                            return response.json().data as Card[]
                        })
                        .catch(this.handleError);
    } 

    public getLatest(): Promise<Card[]> {
        // const url = Constants.serverUrl + 'TestServlet' + '?page=' + Page.get() + '&size=' + Constants.size + '&type=latest';
        const url = Constants.serverUrl + 'content/latest' + '?page=' + Page.get() + '&size=' + Constants.size;
        this.loading = true;

        return this.http.get(url, {withCredentials: true})
                        .toPromise()
                        .then(response => {
                            this.loading = false;
                            return response.json().data as Card[]
                        })
                        .catch(this.handleError);
    }

    public getKinds(): Promise<string[]> {
        let kinds: String[];
        // const url = Constants.serverUrl + 'TestServlet' + '?page=' + Page.get() + '&size=' + Constants.size + '&type=kinds'; 
        const url = Constants.serverUrl + 'content/kinds' + '?page=' + Page.get() + '&size=' + Constants.size; 

        return this.http.get(url, {withCredentials: true})
                        .toPromise()
                        .then(response => {
                            this.loading = false;
                            return response.text().split(Constants.separator) as string[]
                        })
                        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log('>>> ERROR <<<');
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}