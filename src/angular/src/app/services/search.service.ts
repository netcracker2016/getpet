import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Card} from "../entities/card";
import "rxjs/add/operator/toPromise";
import {Constants} from "../constants/constants";
import {SearchQuery} from "../entities/SearchQuery";

@Injectable()
export class SearchService {

    constructor(private http: Http) {

    }

    public getSearchResults(): Promise<Card[]> {
        const url = Constants.serverUrl + 'search/search' + '?page=' + SearchQuery.getPage()
            + '&size=' + Constants.size + '&kind=' + SearchQuery.kind + '&breed=' + SearchQuery.breed +
            '&min_age=' + SearchQuery.minimalAge + '&max_age=' + SearchQuery.maximalAge;

        if ( SearchQuery.isLoading) {
            return SearchQuery.lastResult;
        } else {
            SearchQuery.isLoading = true;
            console.log('URL >>> ' + url);
            SearchQuery.lastResult = this.http.get(url)
                .toPromise()
                .then(response => {
                    SearchQuery.isLoading = false;
                    return response.json().data as Card[]
                })
                .catch(SearchService.handleError);
            return SearchQuery.lastResult;
        }
    }

    public getLatest(): Promise<Card[]> {
        const url = Constants.serverUrl + 'search/latest' + '?page=' + SearchQuery.getPage() + '&size=' + Constants.size;

        if (SearchQuery.isLoading) {
            return SearchQuery.lastResult;
        } else {
            SearchQuery.isLoading = true;
            SearchQuery.lastResult = this.http.get(url, {withCredentials: true})
                .toPromise()
                .then(response => {
                    SearchQuery.isLoading = false;
                    return response.json().data as Card[]
                })
                .catch(SearchService.handleError);
            return SearchQuery.lastResult;
        }
    }

    public getKinds(): Promise<string[]> {
        let kinds: String[];
        const url = Constants.serverUrl + 'search/kinds' + '?page=' + SearchQuery.getPage() + '&size=' + Constants.size;
        SearchQuery.isLoading = true;

        return this.http.get(url, {withCredentials: true})
                        .toPromise()
                        .then(response => {
                            SearchQuery.isLoading = false;
                            return response.text().split(Constants.separator) as string[]
                        })
                        .catch(SearchService.handleError);
    }

    private static handleError(error: any): Promise<any> {
        console.log('>>> ERROR <<<');
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}