import {Injectable} from "@angular/core";
import {Constants} from "../constants/constants";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class PosterService {
    private posterUrl = Constants.serverUrl + "api/poster";
    private posterStatusUpdateUrl = Constants.serverUrl + "api/poster/status";
    private userInfoUrl = Constants.serverUrl + "api/userinfo";
    private petUrl = Constants.serverUrl + "api/pet";

    constructor(private http: Http) {
    }

    getUserInfo(id: string): Observable<Response> {
        return this.getInfo(id, this.userInfoUrl);
    }

    getPoster(id: string): Observable<Response> {
        return this.getInfo(id, this.posterUrl);
    }

    getPet(id: string): Observable<Response> {
        return this.getInfo(id, this.petUrl);
    }

    getInfo(id: string, url: string): Observable<Response> {
        let paramUrl: string = url + "?id=" + id;
        return this.http
            .get(paramUrl, {
                withCredentials: true,
            });
    }

    updateStatusAdvert(id: number, status: string): Observable<Response> {
        let paramUrl: string = this.posterStatusUpdateUrl + "?id=" + id + "&status=" + status;
        return this.http
            .post(paramUrl, null, {
                withCredentials: true,
            });
    }
}
