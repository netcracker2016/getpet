import {Injectable} from "@angular/core";
import {Headers, Http, Response} from "@angular/http";
import {Constants} from "../constants/constants";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthenticationService {

    private loginUrl: string = Constants.serverUrl + "api/login";
    private logoutUrl: string = Constants.serverUrl + "api/logout";
    private registrationUrl: string = Constants.serverUrl + "api/registration";
    private testUrl: string = Constants.serverUrl + "api/test";
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
    }

    // FIXME: not implemented
    isAuthenticated(): boolean {
        return false;
    }

    login(username: string, password: string): Promise<number> {
        let loginDTO: string = JSON.stringify({
            login: username,
            password: password
        });
        return this.http
            .post(this.loginUrl, loginDTO, {headers: this.headers, withCredentials: true})
            .toPromise()
            .then(
                answer => answer.status
            ).catch(
                answer => {
                    console.log(answer.toString());
                    return answer.status;
                })
    }

    logout(): Promise<number> {
        return this.http
            .post(this.logoutUrl, null, {withCredentials: true})
            .toPromise()
            .then(
                answer => answer.status
            ).catch(
                answer => {
                    console.log(answer.toString());
                    return answer.status;
                })
    }

    register(login: string, password: string, email?: string, phone?: string, birthday?: string): Observable<Response> {
        let registrationDTO: string = JSON.stringify({
            login: login,
            password: password,
            email: email,
            phone: phone,
            birthday: birthday
        });
        return this.http
            .post(this.registrationUrl, registrationDTO, {headers: this.headers, withCredentials: true});
    }

    testAuthentication(): Promise<any> {
        return this.http
            .post(this.testUrl, null, {withCredentials: true})
            .toPromise()
            .then(
                answer => answer.text()
            ).catch(
                answer => {
                    if (answer.status == 403) {
                        return "not authenticated";
                    } else {
                        return answer.message;
                    }
                })
    }
}
