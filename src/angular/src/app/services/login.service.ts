import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Constants} from "../constants/constants";

@Injectable()
export class LoginService {

    private testUrl: string = Constants.serverUrl + "api/auth";

    constructor(private http: Http) {
    }

    public static isAuthenticated(): boolean {
        return localStorage.getItem("login") != null;
    }

    public static setUserLogin(login: string) {
        localStorage.setItem("login", login);
    }

    public static getUserLogin(): string {
        return localStorage.getItem("login");
    }

    public static logout() {
        localStorage.clear();
    }

    public checkServerIfLogin(): void {
        this.http.post(
            this.testUrl + "?login=" + LoginService.getUserLogin(),
            null,
            {withCredentials: true}
        ).subscribe(
            response => {
            },
            error => {
                LoginService.logout();
            }
        );
    }
}
