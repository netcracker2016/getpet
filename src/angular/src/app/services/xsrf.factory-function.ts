/**
 * Created by Dmitry Mihaylov on 4/10/17.
 */
import {CookieXSRFStrategy, XSRFStrategy} from "@angular/http";
export function xsrfFactory(): XSRFStrategy {
    return new CookieXSRFStrategy("XSRF-TOKEN", "X-XSRF-TOKEN");
}