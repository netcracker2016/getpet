import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {Constants} from "../constants/constants";
import {Advert} from "../components/add_poster/entities/poster";
/**
 * Created by saveta on 08.05.17.
 */

@Injectable()
export class AddAdvertService {
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  saveAdvert(advert: Advert):Promise<boolean> {
    let url = Constants.serverUrl + 'add_poster/';
    return this.http.post(url, JSON.stringify(advert), {headers: this.headers, withCredentials: true})
      .toPromise()
      .then(() => true)
      .catch(AddAdvertService.handleError);
  }

  static handleError(error: any): Promise<any> {
    console.log('>>> ERROR <<<');
    console.log('My Error', error);
    return Promise.reject(error.message || error);
  }

}
