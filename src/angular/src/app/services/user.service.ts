import {Injectable} from "@angular/core";
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {User} from "../entities/user";
import {Constants} from "../constants/constants";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {

    private petListUrl = Constants.serverUrl + "api/pet/list";

    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {

    }

  public getUserInfo(login: string): Promise<User> {
    let url = Constants.serverUrl + 'user/' + login;
    return this.http.get(url, {withCredentials: true})
      .toPromise()
      .then(response => response.json().data as User)
      .catch(UserService.handleError);
  }

    update(user: User): Promise<boolean> {
        const url = `${Constants.serverUrl}user/${user.login}/edit`;
        return this.http
            .put(url, JSON.stringify(user), {headers: this.headers, withCredentials: true})
            .toPromise()
            .then(() => true)
            .catch(UserService.handleError);
    }

    static handleError(error: any): Promise<any> {
        console.log('>>> ERROR <<<');
        console.log('My Error', error);
        return Promise.reject(error.message || error);
    }

    getPets(login: string): Observable<Response> {
        let paramUrl: string = this.petListUrl + "?login=" + login;
        return this.http
            .get(paramUrl, {
                withCredentials: true,
            });
    }
}
