import {HttpModule, XSRFStrategy} from "@angular/http";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {AppRoutingModule} from "./app-routing.module";

import {AppComponent} from "./app.component";
import {LoginComponent} from "./components/login/login.component";
import {RegistrationComponent} from "./components/registration/registration.component";
import {TestComponent} from "./components/test/test.component";
import {LoginService} from "./services/login.service";
import {AuthGuard} from "./services/auth-guard.service";
import {ContentComponent} from "./components/content/content.component";
import {ResultsComponent} from "./components/results/results.component";
import {CardComponent} from "./components/card/card.component";
import {SearchComponent} from "./components/search/search.component";
import {SearchService} from "./services/search.service";
import {InfoComponent} from "./components/info/info.component";
import {UserEditComponent} from "./components/edit/user-edit.component";
import {UserService} from "./services/user.service";
import {HomeComponent} from "./components/home/home.component";
import {AuthenticationService} from "./services/authentication.service";
import {xsrfFactory} from "./services/xsrf.factory-function";
import {PosterComponent} from "./components/poster/poster.component";
import {Carousel} from "./components/poster/components/carousel/carousel.component";
import {PosterService} from "./services/poster.service";
import {NavigationComponent} from "./components/navigation/navigation.component";
import {AboutComponent} from "./components/about/about.component";
import {AddPosterComponent} from "./components/add_poster/add-poster.component";
import {AddAdvertService} from "./services/add-poster.service";
// import {FileDropDirective} from "ng2-file-upload";
// import {FileUploadModule} from "ng2-file-upload/file-upload/file-upload.module";
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import {FileUploadComponent} from "./components/add_poster/components/file-upload/file-upload.component";
import { FileUploadModule } from "ng2-file-upload";
// import { FileUploadModule } from 'ng2-file-upload/file-upload/file-upload.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        FileUploadModule,
        NgbModule.forRoot(),
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        TestComponent,
        RegistrationComponent,
        ContentComponent,
        ResultsComponent,
        CardComponent,
        SearchComponent,
        InfoComponent,
        UserEditComponent,
        HomeComponent,
        PosterComponent,
        Carousel,
        NavigationComponent,
        AboutComponent,
        AddPosterComponent,
        FileUploadComponent,
    ],
    providers: [
        LoginService,
        AuthGuard,
        UserService,
        SearchService,
        AuthenticationService,
        PosterService,
        AddAdvertService,
        {provide: XSRFStrategy, useFactory: xsrfFactory}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
