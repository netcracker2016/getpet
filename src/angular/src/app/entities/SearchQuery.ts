import {Card} from "./card";

export class SearchQuery {
    static lastResult: Promise<Card[]>;
    static isLoading: boolean = false;
    static error: boolean = false;
    static page: number = 1;

    static kind: string = "";
    static breed: string = "";
    static minimalAge: number = 0;
    static maximalAge: number = 0;

    static getPage() {
        return this.page;
    }

    static increasePage() {
        this.page++;
    }

    static reset() {
        this.page = 1;
        SearchQuery.kind = "";
        SearchQuery.breed = "";
        SearchQuery.minimalAge = 0;
        SearchQuery.maximalAge = 0;
        SearchQuery.isLoading = false;
        SearchQuery.error = false;
    }
}