/**
 * Created by Dmitry Mihaylov on 4/30/17.
 */

export class Pet {
    private _id: number;
    private _name: string;
    private _kind: string;
    private _breed: string;
    private _age: number;
    private _given: boolean;
    private _ownerId: number;
    private _images: string[];


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get kind(): string {
        return this._kind;
    }

    set kind(value: string) {
        this._kind = value;
    }

    get breed(): string {
        return this._breed;
    }

    set breed(value: string) {
        this._breed = value;
    }

    get age(): number {
        return this._age;
    }

    set age(value: number) {
        this._age = value;
    }

    get given(): boolean {
        return this._given;
    }

    set given(value: boolean) {
        this._given = value;
    }

    get ownerId(): number {
        return this._ownerId;
    }

    set ownerId(value: number) {
        this._ownerId = value;
    }

    get images(): string[] {
        return this._images;
    }

    set images(value: string[]) {
        this._images = value;
    }
}