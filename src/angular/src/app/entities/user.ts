/*
 *
 * `id` int(11) NOT NULL AUTO_INCREMENT,
 `login` varchar(45) NOT NULL,
 `password` varchar(45) NOT NULL,
 `role` varchar(45) NOT NULL DEFAULT 'user',
 `email` varchar(45) DEFAULT NULL,
 `phone` varchar(45) DEFAULT NULL,
 `birthday` date DEFAULT NULL,
 `registration_date` date DEFAULT NULL
 */

export class User {
  login: string;
  email: string;
  phone: string;
  birthday: string;

  constructor(login: string, email: string, phone: string, birthday: string) {
    this.login = login;
    this.email = email;
    this.phone = phone;
    this.birthday = birthday;
  }
}
