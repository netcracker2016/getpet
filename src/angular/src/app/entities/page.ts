export class Page {
    private static page: number = 1;

    static get() {
        return this.page;
    }

    static increase() {
        this.page++;
    }

    static reset() {
        this.page = 1;
    }
}