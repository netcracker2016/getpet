/**
 * Created by Dmitry Mihaylov on 4/27/17.
 */

export class Poster {
    private _id: number;
    private _authorId: number;
    private _text: string;
    private _status: string;
    private _petId: number;
    private _startDate: string;


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get authorId(): number {
        return this._authorId;
    }

    set authorId(value: number) {
        this._authorId = value;
    }

    get text(): string {
        return this._text;
    }

    set text(value: string) {
        this._text = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get petId(): number {
        return this._petId;
    }

    set petId(value: number) {
        this._petId = value;
    }

    get startDate(): string {
        return this._startDate;
    }

    set startDate(value: string) {
        this._startDate = value;
    }
}
