export class Card {
    private _kind: string;
    private _breed: string;
    private _age: number;
    private _name: string;
    private _image: string;
    private _advertId: number;

    constructor(kind: string, breed: string, age: number, image: string, advertId: number, name:string) {
        this._kind = kind;
        this._breed = breed;
        this._age = age;
        this._image = image;
        this._advertId = advertId;
        this._name = name;
    }

    public get kind(): string {
        return this._kind;
    }

    public set kind(_kind: string) {
        this._kind = _kind;
    }

    public get breed(): string {
        return this._breed;
    }

    public set breed(_breed: string) {
        this._breed = _breed;
    }

    public get age(): number {
        return this._age;
    }

    public set age(_age: number) {
        this._age = _age;
    }

    public get name(): string {
        return this._name;
    }

    public set name(_name: string) {
        this._name = _name;
    }

    public get image(): string {
        return this._image;
    }

    public set image(_image: string) {
        this._image = _image;
    }

    public get advertId(): number {
        return this._age;
    }

    public set advertId(_advertId: number) {
        this._advertId = _advertId;
    }
}